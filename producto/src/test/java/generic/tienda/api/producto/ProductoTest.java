package generic.tienda.api.producto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import generic.tienda.bean.BlockBean;
import generic.tienda.bean.CategoriaBean;
import generic.tienda.bean.ProductoBean;
import generic.tienda.bean.UpdateBlockBean;
import generic.tienda.bean.type.BlockType;
import generic.tienda.bean.type.ProductoType;
import generic.tienda.bean.util.SimpleKeyBlock;
import io.quarkus.test.junit.QuarkusTest;
import io.vertx.core.json.JsonObject;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@QuarkusTest
public class ProductoTest {
    private static final Logger LOGGER = Logger.getLogger(ProductoTest.class);


    private static String DB_NAME = "generic-tienda";
    private static String PRODUCTO_NAME = "producto";

    @Inject
    MongoClient mongoClient;

    @BeforeEach
    public void setup() {
        LOGGER.infof("Borrando Base de datos: %s", DB_NAME);
        this.getDatabase().drop();
    }

    @Test
    public void insertartProductoTest() {

        ProductoBean productoBean = this.create();

        given()
                .contentType("application/json")
                .body(productoBean)
                .when().post("/producto")
                .then()
                .statusCode(202);

        this.imprimeTodos();

    }

    @Test
    public void buscarProductoPorIdTest() {
        this.insertartProductoTest();
        Document producto = mongoClient.getDatabase(DB_NAME).getCollection(PRODUCTO_NAME).find().first();
        String idProducto = producto.get("_id").toString();

        given()
                .pathParam("id", idProducto)
                .contentType("application/json")
                .when()
                .get("/producto/id/{id}")
                .then()
                .statusCode(200)
                .body("_id", equalTo(idProducto));
    }

    @Test
    public void buscarProductoPorIdCategoriaTest() {
        this.insertartProductoTest();
        Document producto = mongoClient.getDatabase(DB_NAME).getCollection(PRODUCTO_NAME).find().first();
        String idCategoria = producto.get("idCategoria").toString();
        String idProducto = producto.get("_id").toString();

        given()
                .pathParam("idCategoria", idCategoria)
                .contentType("application/json")
                .when()
                .get("/producto/categoria/{idCategoria}")
                .then()
                .statusCode(200)
                .body("[0]._id", equalTo(idProducto));
    }

    @Test
    public void updateProductoPrecioTest() {
        this.insertartProductoTest();
        final String precioName = "precio";
        Document producto = mongoClient.getDatabase(DB_NAME).getCollection(PRODUCTO_NAME).find().first();
        String idProducto = producto.get("_id").toString();

        Random rand = new Random();
        double randonPrecio = rand.nextDouble();
        LOGGER.infof("Precio generado para updatear producto: %s", randonPrecio);

        ProductoBean productoBean = new ProductoBean();
        productoBean.setPrecio(randonPrecio);

        given()
                .body(productoBean)
                .pathParam("id", idProducto)
                .contentType("application/json")
                .when()
                .put("/producto/id/{id}")
                .then()
                .statusCode(200);

        BasicDBObject verifyQuery = new BasicDBObject("_id", new ObjectId(idProducto));
        LOGGER.infof("Buscando producto para verificacion con id: %s", idProducto);
        Document verifyProducto = mongoClient.getDatabase(DB_NAME).getCollection(PRODUCTO_NAME).find(verifyQuery).first();
        LOGGER.infof("Producto para verificacion traido: %s", verifyProducto);

        assertThat(verifyProducto.get(precioName), equalTo(randonPrecio));

        this.imprimeTodos();
    }


    @Test
    public void updateProductoblockTest() {
        this.insertartProductoTest();

        Document oldProduct = this.traemeCualquiera();
        final String productId = oldProduct.get("_id").toString();
        final String desc = "Jueguete pequeño";
        final String descFieldName = "descripcion";
        final String attrFieldName = "atributos";
        BasicDBObject blocksQuery = new BasicDBObject();
        blocksQuery.append("productoType", ProductoType.PRODUCTO.toString());

        List<BlockBean> bloquesDeProducto = this.getDatabase().getCollection("block").find(blocksQuery)
                .into(new ArrayList<>())
                .stream()
                .map(Document.class::cast)
                .map(document -> {
                    convertIdIntoString(document);
                    BlockBean blockBean = this.getMapper().convertValue(document, BlockBean.class);
                    return blockBean;
                })
                .collect(Collectors.toList());

        List<Map<String, Object>> blocksForProduct = createBlocksForProduct(this.getMapper(), bloquesDeProducto);

        blocksForProduct
                .stream()
                .forEach(map -> LOGGER.infof("Block created for product: %s", new JsonObject(map).encodePrettily()));
        List<Map<String, Object>> attributes = (List<Map<String, Object>>) blocksForProduct.get(0).get("atributos");
        Map<String, Object> descripcion = attributes
                .stream()
                .filter(map -> map.get(descFieldName) != null)
                .findAny()
                .orElseThrow(null);

        descripcion.put(descFieldName, desc);

        ProductoBean newProduct = new ProductoBean();
        newProduct.setBloques(blocksForProduct);

        given()
                .body(newProduct)
                .pathParam("id", productId)
                .contentType("application/json")
                .when()
                .put("/producto/id/{id}")
                .then()
                .statusCode(200);

        BasicDBObject verifyQuery = new BasicDBObject("_id", new ObjectId(productId));
        LOGGER.infof("Getting product for verification with id: %s", productId);
        Document verifyProductoDocument = mongoClient.getDatabase(DB_NAME).getCollection(PRODUCTO_NAME).find(verifyQuery).first();
        verifyProductoDocument.put("_id", verifyProductoDocument.get("_id").toString());
        LOGGER.infof("Producto para verificacion traido: %s", new JsonObject(verifyProductoDocument).encodePrettily());

        ProductoBean verifyProdctBean = this.getMapper().convertValue(verifyProductoDocument, ProductoBean.class);
        assertThat(verifyProdctBean, notNullValue());
        LOGGER.infof("Verify prodct bean: %s", verifyProdctBean);
        //List<Document> selectedBlock = verifyProdctBean.getBloques()
        String verifyDesc = verifyProdctBean.getBloques()
                .stream()
                .filter(block -> block
                        .keySet()
                        .stream()
                        .filter(key -> {
                            LOGGER.infof("keyy: %s", key);
                            return key.equals(attrFieldName);
                        })
                        .findAny()
                        .orElse(null) != null)
                .findFirst()
                .map(map -> {
                    LOGGER.infof("block selected: %s", map);
                    List<Map<String, Object>> attrsSelected = (List<Map<String, Object>>) map.get(attrFieldName);
                    String descFiltered = (String) attrsSelected
                            .stream()
                            .filter(mapDesc -> {
                                return mapDesc.keySet()
                                        .stream()
                                        .filter(key -> {
                                            return  key.equals(descFieldName);
                                        })
                                        .findFirst()
                                        .orElse(null) != null;
                            })
                            .findFirst()
                            .orElseThrow(null)
                            .get(descFieldName);

                    return descFiltered;
                })
                .orElseThrow(null);
            //    .collect(Collectors.toList())
                ;

        LOGGER.infof("Block to verify: %s", verifyDesc);

        assertThat(verifyDesc, notNullValue());
        assertThat(verifyProdctBean.get_id(), equalTo(productId));
        assertThat(verifyDesc, equalTo(desc));


        this.imprimeTodos();


    }


    private void imprimeTodos() {
        this.traemelosTodos()
                .stream()
                .map(Document.class::cast)
                .map(document -> {
                    document.put("_id", document.get("_id").toString());
                    return new JsonObject(document);
                })
                .forEach(json -> {
                    LOGGER.info(json.encodePrettily());
                });
    }

    private MongoCollection<Document> getCollection() {
        return this.getDatabase().getCollection(PRODUCTO_NAME);
    }

    private MongoDatabase getDatabase() {
        return mongoClient.getDatabase(DB_NAME);
    }

    private Document traemeCualquiera() {
        return this.getCollection().find().first();
    }

    private List<Document> traemelosTodos() {
        return this.getCollection().find().into(new ArrayList<>());
    }

    private List<BlockBean> createBlocks() {

        List<BlockBean> blocks = new ArrayList<>();

        SimpleKeyBlock contenidoAtributosBlock = new SimpleKeyBlock();
        contenidoAtributosBlock.addKey("nombre");
        contenidoAtributosBlock.addKey("descripcion");
        contenidoAtributosBlock.addKey("color");
        contenidoAtributosBlock.addKey("peso");

        BlockBean atributosBlock = new BlockBean();
        atributosBlock.setBlockType(BlockType.SIMPLE_OBJECT);
        atributosBlock.setNombre("atributos");
        atributosBlock.setProductoType(ProductoType.PRODUCTO);
        atributosBlock.setContenidoObject(contenidoAtributosBlock);

        blocks.add(atributosBlock);
        return blocks;
    }

    private void createAndSaveBlocks(ObjectMapper mapper) {
        List<BlockBean> blocks = this.createBlocks();

        this.getDatabase().getCollection("block").insertMany(blocks
                .stream()
                .map(blockBean -> mapper.convertValue(blockBean, Document.class))
                .collect(Collectors.toList()));
    }

    private CategoriaBean createCategory() {
        List<String> imagenes = new ArrayList<>();
        imagenes.add("https://i.picsum.photos/id/50/600/800.jpg");
        imagenes.add("https://i.picsum.photos/id/51/600/800.jpg");
        imagenes.add("https://i.picsum.photos/id/52/600/800.jpg");
        imagenes.add("https://i.picsum.photos/id/53/600/800.jpg");

        CategoriaBean categoriaBean = new CategoriaBean();
        categoriaBean.setImagenes(imagenes);
        categoriaBean.setNombre("Juguetes");

        return categoriaBean;
    }

    private CategoriaBean createAndSaveCategoria(ObjectMapper mapper) {
        CategoriaBean categoriaBean = this.createCategory();

        Document insert = mapper.convertValue(categoriaBean, Document.class);
        this.getDatabase()
                .getCollection("categoria")
                .insertOne(insert);
        LOGGER.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        LOGGER.info(insert);
        categoriaBean.set_id(insert.get("_id").toString());
        return categoriaBean;
    }

    private ObjectMapper getMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper;
    }

    private void convertIdIntoString(Document document) {
        document.put("_id", document.get("_id").toString());
    }

    private ProductoBean create() {

        ObjectMapper mapper = this.getMapper();
        CategoriaBean categoriaBean = createAndSaveCategoria(mapper);
        createAndSaveBlocks(mapper);

        BasicDBObject blocksQuery = new BasicDBObject();
        blocksQuery.append("productoType", ProductoType.PRODUCTO.toString());
        List<BlockBean> bloquesDeProducto = this.getDatabase().getCollection("block").find(blocksQuery)
                .into(new ArrayList<>())
                .stream()
                .map(Document.class::cast)
                .map(document -> {
                    convertIdIntoString(document);
                    BlockBean blockBean = mapper.convertValue(document, BlockBean.class);
                    return blockBean;
                })
                .collect(Collectors.toList());

        List<Map<String, Object>> newBlocks = createBlocksForProduct(mapper, bloquesDeProducto);

        List<String> carateristicas = new ArrayList<>();
        carateristicas.add("Rebota");
        carateristicas.add("Suave");
        carateristicas.add("Para los más pequeños");

        ProductoBean productoBean = new ProductoBean(categoriaBean.get_id(), 200, ProductoType.PRODUCTO, newBlocks, carateristicas);
        List<String> imagenes = new ArrayList<>();
        imagenes.add("https://i.picsum.photos/id/76/600/800.jpg");
        imagenes.add("https://i.picsum.photos/id/77/600/800.jpg");
        imagenes.add("https://i.picsum.photos/id/78/600/800.jpg");
        imagenes.add("https://i.picsum.photos/id/79/600/800.jpg");
        imagenes.add("https://i.picsum.photos/id/75/600/800.jpg");

        productoBean.setImagenes(imagenes);
        return productoBean;
    }

    private List<Map<String, Object>> createBlocksForProduct(ObjectMapper mapper, List<BlockBean> bloquesDeProducto) {
        List<Map<String, Object>> newBlocks = new ArrayList<>();
        for (BlockBean bloque : bloquesDeProducto) {
            Map<String, Object> newBloque = new HashMap<>();
            List<Map<String, Object>> newBlocksContent = new ArrayList<>();
            switch (bloque.getBlockType()) {
                case SIMPLE_OBJECT:
                    SimpleKeyBlock contenidoBloque = mapper.convertValue(bloque.obtenerContenidoLleno(), SimpleKeyBlock.class);
                    for (String contenidoBloqueKey : contenidoBloque.getKeys()) {
                        Map<String, Object> newContenido = new HashMap<>();
                        switch (contenidoBloqueKey) {
                            case "nombre":
                                newContenido.put(contenidoBloqueKey, "pelota");
                                break;
                            case "descripcion":
                                newContenido.put(contenidoBloqueKey, "Juguete mediano");
                                break;
                            case "color":
                                newContenido.put(contenidoBloqueKey, "blanco");
                                break;
                            case "peso":
                                newContenido.put(contenidoBloqueKey, "27kg");
                                break;
                            default:
                                break;
                        }
                        newBlocksContent.add(newContenido);
                    }
                default:
                    break;
            }
            newBloque.put(bloque.getNombre(), newBlocksContent);
            newBlocks.add(newBloque);
        }

        return newBlocks;
    }


}
