package generic.tienda.api;


import generic.tienda.bean.ProductoBean;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Path("/producto")
public class ProductoEndpoint {
    private static final Logger LOGGER = Logger.getLogger(ProductoEndpoint.class);

    @Inject
    ProductoService productoService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertarProducto(ProductoBean productoBean) {
        //TODO:Validar antes de guardar
        LOGGER.debugf("Producto: %s", productoBean.toString());
        this.productoService.insertOne(productoBean);
        return Response.accepted().build();
    }

    @PUT
    @Path("/id/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response modificarProducto(@PathParam("id") String id, ProductoBean productoBean) {
        //TODO:Validar antes de guardar
        LOGGER.debugf("Product to update: %s", productoBean.toString());
        if(productoBean.getBloques() != null){
            Set<String> blockKeysToUpdate = productoBean.getBloques()
                    .stream()
                    .flatMap(map -> map.keySet().stream())
                    .collect(Collectors.toSet());

            LOGGER.info("-------------------------------------------------------------------------------------------");
            LOGGER.info("-------------------------------------------------------------------------------------------");
            LOGGER.info(blockKeysToUpdate);
            LOGGER.info("-------------------------------------------------------------------------------------------");
            LOGGER.info("-------------------------------------------------------------------------------------------");

            this.productoService.saveOldBlocks(blockKeysToUpdate, id);
        }else{
            LOGGER.debug("No blocks to update....");
        }
        this.productoService.saveUpdate(id, productoBean);
        return Response.ok().build();
    }

    @GET()
    @Path("/id/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ProductoBean obtenerProductoPorId(@PathParam("id") String id) {
        LOGGER.debugf("Obteniendo producto por id: %s", id);
        return this.productoService.obtenerPorId(id);
    }

    @GET()
    @Path("/categoria/{idCategoria}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProductoBean> obtenerProductoPorIdCategoria(@PathParam("idCategoria") String idCategoria) {
        LOGGER.debugf("Obteniendo producto por id: %s", idCategoria);
        return this.productoService.obtenerPorIdCategoria(idCategoria);
    }
}
