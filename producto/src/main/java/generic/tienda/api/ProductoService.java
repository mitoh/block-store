package generic.tienda.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Updates;
import generic.tienda.bean.BlockBean;
import generic.tienda.bean.ProductoBean;
import generic.tienda.bean.UpdateBlockBean;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

@ApplicationScoped
public class ProductoService {
    private static final Logger LOGGER = Logger.getLogger(ProductoService.class);

    @Inject
    MongoClient mongoClient;

    @ConfigProperty(name = "old.block.table.name", defaultValue = "updated-blocks")
    String OLD_BLOCK_TABLE_NAME;

    ObjectMapper mapper;

    public void insertOne(ProductoBean productoBean){
        Document insert = this.getMapper().convertValue(productoBean, Document.class);
        this.getCollection().insertOne(insert);
    }

    public void saveUpdate(String id, ProductoBean productoBean){
        BasicDBObject query = new BasicDBObject("_id", new ObjectId(id));
        Document update = this.getMapper().convertValue(productoBean, Document.class);

        List<Bson> updates = new ArrayList<>();
        for (Object o : update.entrySet()) {
            Map.Entry entry = (Map.Entry) o;
            Bson bson = Updates.set((String) entry.getKey(), entry.getValue());
            updates.add(bson);
        }

        LOGGER.debugf("Query: %s y update: %s para updatear documento.", query, update);
        this.getCollection().updateOne(query, Updates.combine(updates));
    }

    public ProductoBean obtenerPorId(String id){
        BasicDBObject query = new BasicDBObject();
        query.append("_id", new ObjectId(id));
        Document result = (Document) this.getCollection().find(query).first();
        this.convertIdIntoString(result);
        ProductoBean productoBean = this.getMapper().convertValue(result, ProductoBean.class);
        return productoBean;
    }

    public List<ProductoBean> obtenerPorIdCategoria(String idcategoria){
        BasicDBObject query = new BasicDBObject();
        query.append("idCategoria", idcategoria);
        List<Document> result = (List<Document>) this.getCollection().find(query).into(new ArrayList());

        List<ProductoBean> productsBean = result
                .stream()
                .map(Document.class::cast)
                .map(document -> this.parse(document))
                .collect(Collectors.toList());

        return productsBean;
    }

    private ProductoBean parse(Document document){
        this.convertIdIntoString(document);
        ProductoBean productoBean = this.getMapper().convertValue(document, ProductoBean.class);
        return  productoBean;
    }

    private void convertIdIntoString(Document document){
        document.put("_id", document.get("_id").toString());
    }

    private ObjectMapper getMapper(){
        if(this.mapper == null){
            this.mapper = new ObjectMapper();
            this.mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }
        return this.mapper;
    }

    private MongoCollection getCollection(){
        return mongoClient.getDatabase("generic-tienda").getCollection("producto");
    }
    private MongoCollection getOldBlockCollection(){
        return mongoClient.getDatabase("generic-tienda").getCollection(OLD_BLOCK_TABLE_NAME);
    }

    public void saveOldBlocks(Set<String> blockKeys, String productId) {

        ProductoBean productoBean = this.obtenerPorId(productId);
        List<Document> blocksToSave = new ArrayList<>();
        for (String blockKey : blockKeys) {
            LOGGER.debugf("Block key to update: %s", blockKey);
            List<Document> selectedBlock = productoBean.getBloques()
                    .stream()
                    .filter(block -> block
                            .keySet()
                            .stream()
                            .filter(key -> key.equals(blockKey))
                            .findAny()
                            .orElseThrow(null) != null)
                    .map(map -> {
                        UpdateBlockBean updateBlockBean = new UpdateBlockBean();
                        updateBlockBean.setOldBlockBean(map);
                        updateBlockBean.setProductId(productId);
                        Document blockToSave = this.getMapper().convertValue(updateBlockBean, Document.class);
                        blockToSave.put("date", new Date());
                        return blockToSave;
                    })
                    .map(Document.class::cast)
                    .collect(Collectors.toList());

            blocksToSave.addAll(selectedBlock);
        }

        LOGGER.debugf("Block selected to convert in old: %s", blocksToSave);

        this.getOldBlockCollection().insertMany(blocksToSave);
    }
}
