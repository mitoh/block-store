package generic.tienda.bean.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimpleKeyBlock {
    private List<String> keys;

    @Override
    public String toString() {
        return "SimpleKeyBlock{" +
                ", keys=" + keys +
                '}';
    }

    /**
     * Add key to Array.
     * Create new instances of key if es null.
     *
     * @param newKey
     * @return inserted key
     */
    public String addKey(String newKey) {
        if (keys == null) {
            this.keys = new ArrayList<String>();
        }
        this.keys.add(newKey);
        return newKey;
    }

    public List<String> getKeys() {
        return keys;
    }

    public void setKeys(List<String> keys) {
        this.keys = keys;
    }
}
