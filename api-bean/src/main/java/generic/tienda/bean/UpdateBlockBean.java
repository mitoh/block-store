package generic.tienda.bean;

import generic.tienda.bean.BlockBean;

import java.util.Date;

public class UpdateBlockBean {
    private String _id;
    private Object oldBlockBean;
    private String productId;
    private Date updateDate;


    @Override
    public String toString() {
        return "UpdateBlockBean{" +
                "_id='" + _id + '\'' +
                ", oldBlockBean=" + oldBlockBean +
                ", productId='" + productId + '\'' +
                ", updateDate=" + updateDate +
                '}';
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Object getOldBlockBean() {
        return oldBlockBean;
    }

    public void setOldBlockBean(Object oldBlockBean) {
        this.oldBlockBean = oldBlockBean;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
