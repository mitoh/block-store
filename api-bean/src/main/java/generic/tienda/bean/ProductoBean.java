package generic.tienda.bean;

import generic.tienda.bean.type.ProductoType;

import java.util.List;
import java.util.Map;


public class ProductoBean {
    private String _id;
    private ProductoType tipo;
    private double precio;
    private List<String> imagenes;
    private String idCategoria;
    private List<String> carecteristicas;
    private List<Map<String, Object>> bloques;

    public ProductoBean() {
    }

    public ProductoBean(String idCategoria, double precio, ProductoType productoType, List<Map<String, Object>> bloques, List<String> caracteristicas) {
        this.idCategoria = idCategoria;
        this.precio = precio;
        this.carecteristicas = caracteristicas;
        this.tipo = productoType;
        this.bloques = bloques;
    }

    @Override
    public String toString() {
        return "ProductoBean{" +
                "_id='" + _id + '\'' +
                ", tipo=" + tipo +
                ", precio=" + precio +
                ", imagenes=" + imagenes +
                ", idCategoria='" + idCategoria + '\'' +
                ", carecteristicas=" + carecteristicas +
                ", bloques=" + bloques +
                '}';
    }

    public List<Map<String, Object>> getBloques() {
        return bloques;
    }

    public void setBloques(List<Map<String, Object>> bloques) {
        this.bloques = bloques;
    }

    public List<String> getImagenes() {
        return imagenes;
    }

    public void setImagenes(List<String> imagenes) {
        this.imagenes = imagenes;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public List<String> getCarecteristicas() {
        return carecteristicas;
    }

    public void setCarecteristicas(List<String> carecteristicas) {
        this.carecteristicas = carecteristicas;
    }

    public ProductoType getTipo() {
        return tipo;
    }

    public void setTipo(ProductoType tipo) {
        this.tipo = tipo;
    }


}
