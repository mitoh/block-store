package generic.tienda.bean.type;

public enum BlockType {
    ARRAY, SIMPLE_OBJECT, CUSTOM_OBJECT
}
