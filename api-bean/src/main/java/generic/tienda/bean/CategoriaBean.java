package generic.tienda.bean;

import java.util.List;

public class CategoriaBean {

    private String _id;
    private String nombre;
    private List<String> imagenes;
    private String idParent;
    private String descripcion;

    @Override
    public String toString() {
        return "CategoriaBean{" +
                "_id='" + _id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", imagenes=" + imagenes +
                ", idParent='" + idParent + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<String> getImagenes() {
        return imagenes;
    }

    public void setImagenes(List<String> imagenes) {
        this.imagenes = imagenes;
    }

    public String getIdParent() {
        return idParent;
    }

    public void setIdParent(String idParent) {
        this.idParent = idParent;
    }
}
