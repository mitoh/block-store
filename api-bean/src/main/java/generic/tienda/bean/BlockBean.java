package generic.tienda.bean;

import generic.tienda.bean.type.BlockType;
import generic.tienda.bean.type.ProductoType;

import java.util.List;
import java.util.Map;

public class BlockBean {
    private ProductoType productoType;
    private BlockType blockType;
    private String _id;
    private String nombre;
    private Map<String, Object> contenidoMap;
    private List<Object> contenidoList;
    private Object contenidoObject;
    private ContenidoLlenoEnum contenidoLleno;

    @Override
    public String toString() {
        return "BlockBean{" +
                "productoType=" + productoType +
                ", blockType=" + blockType +
                ", _id='" + _id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", contenidoMap=" + contenidoMap +
                ", contenidoList=" + contenidoList +
                ", contenidoObject=" + contenidoObject +
                '}';
    }

    public Object obtenerContenidoLleno(){
        switch (this.contenidoLleno){
            case OBJECT:
                return this.contenidoObject;
            case MAP:
                return this.contenidoMap;
            case LIST:
                return this.contenidoList;
            default:
                throw new NullPointerException("No se encuentra el tipo de contenido, o no se ha llenado");
        }
    }

    public ContenidoLlenoEnum getContenidoLleno() {
        return contenidoLleno;
    }

    public Object getContenidoObject() {
        return contenidoObject;
    }

    public void setContenidoObject(Object contenidoObject) {
        this.contenidoObject = contenidoObject;
        this.contenidoLleno = ContenidoLlenoEnum.OBJECT;
    }

    public ProductoType getProductoType() {
        return productoType;
    }

    public void setProductoType(ProductoType productoType) {
        this.productoType = productoType;
    }

    public BlockType getBlockType() {
        return blockType;
    }

    public void setBlockType(BlockType blockType) {
        this.blockType = blockType;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Map<String, Object> getContenidoMap() {
        return contenidoMap;
    }

    public void setCustomContent(Map<String, Object> contenidoMap) {
        this.contenidoLleno = ContenidoLlenoEnum.MAP;
        this.contenidoMap = contenidoMap;
    }


    public List<Object> getContenidoList() {
        return contenidoList;
    }

    public void setContenidoList(List<Object> contenidoList) {
        this.contenidoLleno = ContenidoLlenoEnum.LIST;
        this.contenidoList = contenidoList;
    }


    public enum ContenidoLlenoEnum{
        LIST, MAP, OBJECT;
    }
}
