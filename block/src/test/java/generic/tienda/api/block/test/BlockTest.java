package generic.tienda.api.block.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import generic.tienda.bean.BlockBean;
import generic.tienda.bean.type.BlockType;
import generic.tienda.bean.type.ProductoType;
import generic.tienda.bean.util.SimpleKeyBlock;
import io.quarkus.test.junit.QuarkusTest;
import io.vertx.core.json.JsonObject;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

@QuarkusTest
public class BlockTest {
    private static final Logger LOGGER = Logger.getLogger(BlockTest.class);


    private static String DB_NAME = "generic-tienda";
    private static String BLOCK_NAME = "block";

    @Inject
    MongoClient mongoClient;

    @BeforeEach
    public void setup(){
        LOGGER.infof("Borrando Base de datos: %s", DB_NAME);
        this.getDatabase().drop();
    }

    @Test
    public void insertProductBlockSimpleObjectTest() {

        BlockBean blockBean = this.create();

        given()
                .contentType("application/json")
                .body(blockBean)
                .when().post("/block")
                .then()
                .statusCode(202);

        imprimeTodos();
    }

    public void insertProductBlockComplexObjectTest() {

        BlockBean blockBean = this.create();

        given()
                .contentType("application/json")
                .body(blockBean)
                .when().post("/block")
                .then()
                .statusCode(202);

        imprimeTodos();
    }

    @Test
    public void buscarPorTipoTest(){

        this.insertProductBlockSimpleObjectTest();

        Document block = traemeCualquiera();
        String idBlock = block.get("_id").toString();
        String tipoProducto = block.get("productoType").toString();

        given()
                .pathParam("tipoProducto", tipoProducto)
                .contentType("application/json")
                .when()
                .get("/block/tipoProducto/{tipoProducto}")
                .then()
                .statusCode(200)
                .body("_id", equalTo(idBlock));
    }


    public void buscarCategoriaPorNombreTest(){
        this.insertProductBlockSimpleObjectTest();

        Document categoria = traemeCualquiera();
        String nombreCategoria = categoria.get("nombre").toString();
        String idCategoria = categoria.get("_id").toString();

        given()
                .pathParam("nombreCat", nombreCategoria)
                .contentType("application/json")
                .when()
                .get("/categoria/nombre/{nombreCat}")
                .then()
                .statusCode(200)
                .body("_id", equalTo(idCategoria));
    }


    @Test
    public void updateBlockTest(){
        final String fieldName = "nombre";
        this.insertProductBlockSimpleObjectTest();

        Document oldBlock = this.traemeCualquiera();
        String olIdBlock = oldBlock.get("_id").toString();

        final String newBlockName = "Especificaciones";
        BlockBean newBloclk = new BlockBean();

        newBloclk.setNombre(newBlockName);

        SimpleKeyBlock newContenidoAtributosBlock = new SimpleKeyBlock();
        newContenidoAtributosBlock.addKey("Altura");
        newContenidoAtributosBlock.addKey("Anchura");
        newContenidoAtributosBlock.addKey("Capacidad");
        newContenidoAtributosBlock.addKey("Material");
        newBloclk.setContenidoObject(newContenidoAtributosBlock);

        given()
                .body(newBloclk)
                .pathParam("id", olIdBlock)
                .contentType("application/json")
                .when()
                .put("/block/id/{id}")
                .then()
                .statusCode(200);

        BasicDBObject verifyQuery = new BasicDBObject("_id", new ObjectId(olIdBlock));
        LOGGER.infof("Finding block to verification with id: %s", olIdBlock);
        Document verifyBlock = mongoClient.getDatabase(DB_NAME).getCollection(BLOCK_NAME).find(verifyQuery).first();
        LOGGER.infof("Block for verification obtained: %s", verifyBlock);

        assertThat(verifyBlock.get(fieldName), equalTo(newBlockName));

        Map<String, Object> contentObject = (Map<String, Object>) verifyBlock.get("contenidoObject");
        List<String> keysVerify = (List<String>) contentObject.get("keys");
        assertThat(keysVerify.size(), equalTo(newContenidoAtributosBlock.getKeys().size()));

        for (String keyVerify : keysVerify) {
            String keyObtained = newContenidoAtributosBlock.getKeys()
                    .stream()
                    .filter(newKey -> keyVerify.equals(newKey))
                    .findFirst()
                    .orElseThrow(null);
            assertThat(keyObtained, notNullValue());
        }
        keysVerify.removeAll(newContenidoAtributosBlock.getKeys());
        assertThat(keysVerify.size(), equalTo(0));
    }

    private void imprimeTodos() {
        this.traemelosTodos()
                .stream()
                .map(Document.class::cast)
                .map(document -> {
                    document.put("_id", document.get("_id").toString());
                    return new JsonObject(document);
                })
                .forEach(json -> {
                    LOGGER.info(json.encodePrettily());
                });
    }

    private ObjectMapper getMapper(){
        ObjectMapper mapper = new ObjectMapper();
        return mapper;
    }

    private Document traemeCualquiera() {
        return this.getCollection().find().first();
    }
    private List<Document> traemelosTodos() {
        return this.getCollection().find().into(new ArrayList<>());
    }


    private MongoCollection<Document> getCollection(){
        return this.getDatabase().getCollection(BLOCK_NAME);
    }
    private MongoDatabase getDatabase(){
        return mongoClient.getDatabase(DB_NAME);
    }

    private BlockBean create(){

        SimpleKeyBlock contenidoAtributosBlock = new SimpleKeyBlock();
        contenidoAtributosBlock.addKey("nombre");
        contenidoAtributosBlock.addKey("descripcion");
        contenidoAtributosBlock.addKey("color");
        contenidoAtributosBlock.addKey("peso");

        BlockBean atributosBlock = new BlockBean();
        atributosBlock.setBlockType(BlockType.SIMPLE_OBJECT);
        atributosBlock.setNombre("atributos");
        atributosBlock.setProductoType(ProductoType.PRODUCTO);
        atributosBlock.setContenidoObject(contenidoAtributosBlock);

        return atributosBlock;
    }

    private BlockBean createForCustomObject(){

        SimpleKeyBlock contenidoAtributosBlock = new SimpleKeyBlock();

        JsonObject customSchema = new JsonObject();
        customSchema
                .put("key", new JsonObject().put("type", "string"))
                .put("dates", new JsonObject().put("type", "array"));

//
//        contenidoAtributosBlock.addKey("nombre");
//        contenidoAtributosBlock.addKey("descripcion");
//        contenidoAtributosBlock.addKey("color");
//        contenidoAtributosBlock.addKey("peso");

        BlockBean atributosBlock = new BlockBean();
//        atributosBlock.setBlockType(BlockType.CUSTOM_OBJECT);
//        atributosBlock.setNombre("Calendar");
//        atributosBlock.setProductoType(ProductoType.SERVICE);
//        atributosBlock.setContenidoObject(contenidoAtributosBlock);

        return atributosBlock;
    }




}
