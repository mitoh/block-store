package generic.tienda.api.block;


import generic.tienda.api.categoria.BlockService;
import generic.tienda.bean.BlockBean;
import generic.tienda.bean.CategoriaBean;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/block")
public class BlockEndpoint {
    private static final Logger LOGGER = Logger.getLogger(BlockEndpoint.class);

    @Inject
    BlockService blockService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertarBlock(BlockBean blockBean) {
        //TODO:Validar antes de guardar
        LOGGER.debugf("Bloque: %s", blockBean.toString());
        this.blockService.insertOne(blockBean);
        return Response.accepted().build();
    }

    @PUT
    @Path("/id/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response modificarBlock(@PathParam("id") String id, BlockBean blockBean) {
        //TODO:Validar antes de guardar
        LOGGER.debugf("Block: %s", blockBean.toString());
        this.blockService.update(id, blockBean);
        return Response.ok().build();
    }

    @GET()
    @Path("/tipoProducto/{tipoProducto}")
    @Produces(MediaType.APPLICATION_JSON)
    public BlockBean obtenerCategoriaPorId(@PathParam("tipoProducto") String tipoProducto) {
        LOGGER.debugf("Obteniendo block por tipo de producto: %s", tipoProducto);
        return this.blockService.obtenerPorTipoProducto(tipoProducto);
    }

    @GET()
    @Path("/nombre/{nombreCategoria}")
    @Produces(MediaType.APPLICATION_JSON)
    public CategoriaBean obtenerProductoPorIdCategoria(@PathParam("nombreCategoria") String nombreCategoria) {
        LOGGER.debugf("Obteniendo categoria por nombre: %s", nombreCategoria);
        return this.blockService.obtenerPorNombre(nombreCategoria);
    }
}
