package generic.tienda.api.categoria;


import generic.tienda.bean.CategoriaBean;
import generic.tienda.bean.ProductoBean;
import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/categoria")
public class CategoriaEndpoint {
    private static final Logger LOGGER = Logger.getLogger(CategoriaEndpoint.class);

    @Inject
    CategoriaService categoriaService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertarCategoria(CategoriaBean categoriaBean) {
        //TODO:Validar antes de guardar
        LOGGER.debugf("categoria: %s", categoriaBean.toString());
        this.categoriaService.insertOne(categoriaBean);
        return Response.accepted().build();
    }

    @PUT
    @Path("/id/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response modificarCategoria(@PathParam("id") String id, CategoriaBean categoriaBean) {
        //TODO:Validar antes de guardar
        LOGGER.debugf("Categoria: %s", categoriaBean.toString());
        this.categoriaService.updatear(id, categoriaBean);
        return Response.ok().build();
    }

    @GET()
    @Path("/id/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public CategoriaBean obtenerCategoriaPorId(@PathParam("id") String id) {
        LOGGER.debugf("Obteniendo categoria por id: %s", id);
        return this.categoriaService.obtenerPorId(id);
    }

    @GET()
    @Path("/nombre/{nombreCategoria}")
    @Produces(MediaType.APPLICATION_JSON)
    public CategoriaBean obtenerProductoPorIdCategoria(@PathParam("nombreCategoria") String nombreCategoria) {
        LOGGER.debugf("Obteniendo categoria por nombre: %s", nombreCategoria);
        return this.categoriaService.obtenerPorNombre(nombreCategoria);
    }
}
