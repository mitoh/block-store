package generic.tienda.api.categoria;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Updates;
import generic.tienda.bean.CategoriaBean;
import generic.tienda.bean.ProductoBean;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class CategoriaService {
    private static final Logger LOGGER = Logger.getLogger(CategoriaService.class);

    @Inject
    MongoClient mongoClient;

    ObjectMapper mapper;

    public void insertOne(CategoriaBean categoriaBean){
        Document insert = this.getMapper().convertValue(categoriaBean, Document.class);
        this.getCollection().insertOne(insert);
    }

    public void updatear(String id, CategoriaBean categoriaBean){
        BasicDBObject query = new BasicDBObject("_id", new ObjectId(id));
        Document update = this.getMapper().convertValue(categoriaBean, Document.class);

        List<Bson> updates = new ArrayList<>();
        for (Object o : update.entrySet()) {
            Map.Entry entry = (Map.Entry) o;
            Bson bson = Updates.set((String) entry.getKey(), entry.getValue());
            updates.add(bson);
        }

        LOGGER.debugf("Query: %s y update: %s para updatear documento.", query, update);
        this.getCollection().updateOne(query, Updates.combine(updates));
    }

    public CategoriaBean obtenerPorId(String id){
        BasicDBObject query = new BasicDBObject();
        query.append("_id", new ObjectId(id));
        Document result = (Document) this.getCollection().find(query).first();
        LOGGER.debugf("Obtaining categoy: " + result);
        this.convertIdIntoString(result);
        CategoriaBean categoriaBean = this.getMapper().convertValue(result, CategoriaBean.class);
        return categoriaBean;
    }

    public CategoriaBean obtenerPorNombre(String nombreCategoria){
        BasicDBObject query = new BasicDBObject();
        query.append("nombre", nombreCategoria);
        Document result = (Document) this.getCollection().find(query).first();
        CategoriaBean categoriaBean = this.parse(result);
        return categoriaBean;
    }

    private CategoriaBean parse(Document document){
        this.convertIdIntoString(document);
        CategoriaBean categoriaBean = this.getMapper().convertValue(document, CategoriaBean.class);
        return  categoriaBean;
    }

    private void convertIdIntoString(Document document){
        if(document != null){
            document.put("_id", document.get("_id").toString());
        }
    }

    private ObjectMapper getMapper(){
        if(this.mapper == null){
            this.mapper = new ObjectMapper();
            this.mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        }
        return this.mapper;
    }

    private MongoCollection getCollection(){
        return mongoClient.getDatabase("generic-tienda").getCollection("categoria");
    }
}
