package generic.tienda.api.block.test;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import generic.tienda.bean.CategoriaBean;
import io.quarkus.test.junit.QuarkusTest;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
public class CategoriaTest {
    private static final Logger LOGGER = Logger.getLogger(CategoriaTest.class);


    private static String DB_NAME = "generic-tienda";
    private static String PRODUCTO_NAME = "categoria";

    @Inject
    MongoClient mongoClient;

    @BeforeEach
    public void setup(){
        LOGGER.infof("Borrando Base de datos: %s", DB_NAME);
        this.getDatabase().drop();
    }

    @Test
    public void insertartCategoriaTest() {

        CategoriaBean categoriaBean = this.create();

        given()
                .contentType("application/json")
                .body(categoriaBean)
                .when().post("/categoria")
                .then()
                .statusCode(202);
    }


    @Test
    public void buscarCategoriaPorIdTest(){

        this.insertartCategoriaTest();

        Document categoria = traemeCualquiera();
        String idCategoria = categoria.get("_id").toString();

        given()
                .pathParam("id", idCategoria)
                .contentType("application/json")
                .when()
                .get("/categoria/id/{id}")
                .then()
                .statusCode(200)
                .body("_id", equalTo(idCategoria));
    }


    @Test
    public void buscarCategoriaPorNombreTest(){
        this.insertartCategoriaTest();

        Document categoria = traemeCualquiera();
        String nombreCategoria = categoria.get("nombre").toString();
        String idCategoria = categoria.get("_id").toString();

        given()
                .pathParam("nombreCat", nombreCategoria)
                .contentType("application/json")
                .when()
                .get("/categoria/nombre/{nombreCat}")
                .then()
                .statusCode(200)
                .body("_id", equalTo(idCategoria));
    }

    private Document traemeCualquiera() {
        return this.getCollection().find().first();
    }


    @Test
    public void updateCategoriaTest(){
        final String nombre = "nombre";
        this.insertartCategoriaTest();

        Document oldCategoria = this.traemeCualquiera();
        String idOldCategoria = oldCategoria.get("_id").toString();

        final String nuevoNombre = "Ropa";
        CategoriaBean newCategoria = new CategoriaBean();
        newCategoria.setNombre(nuevoNombre);
        List<String> oldImagenes = (List<String>) oldCategoria.get("imagenes");
        oldImagenes.remove(0);
        int originalImagenesSizeBeforeRemoveItem = oldImagenes.size();
        newCategoria.setImagenes(oldImagenes);

        given()
                .body(newCategoria)
                .pathParam("id", idOldCategoria)
                .contentType("application/json")
                .when()
                .put("/categoria/id/{id}")
                .then()
                .statusCode(200);

        BasicDBObject verifyQuery = new BasicDBObject("_id", new ObjectId(idOldCategoria));
        LOGGER.infof("Buscando categoria para verificacion con id: %s", idOldCategoria);
        Document verifyProducto = mongoClient.getDatabase(DB_NAME).getCollection(PRODUCTO_NAME).find(verifyQuery).first();
        LOGGER.infof("Producto para verificacion traido: %s", verifyProducto);

        assertThat(verifyProducto.get(nombre), equalTo(nuevoNombre));

        List<String> newImagenes = (List<String>) oldCategoria.get("imagenes");
        int newImagenesSize = newImagenes.size();

        assertThat(originalImagenesSizeBeforeRemoveItem, equalTo(newImagenesSize));
    }

    private MongoCollection<Document> getCollection(){
        return this.getDatabase().getCollection(PRODUCTO_NAME);
    }
    private MongoDatabase getDatabase(){
        return mongoClient.getDatabase(DB_NAME);
    }

    private CategoriaBean create(){
        List<String> imagenes = new ArrayList<>();
        imagenes.add("https://i.picsum.photos/id/50/600/800.jpg");
        imagenes.add("https://i.picsum.photos/id/51/600/800.jpg");
        imagenes.add("https://i.picsum.photos/id/52/600/800.jpg");
        imagenes.add("https://i.picsum.photos/id/53/600/800.jpg");

        CategoriaBean categoriaBean = new CategoriaBean();
        categoriaBean.setImagenes(imagenes);
        categoriaBean.setNombre("Juguetes");

        return categoriaBean;
    }




}
