#!/bin/bash

# $1: true=runs mvn package system, false/nothing=does nothing
# $2 (need $1 in true/false): true=runs with native mode, false/nothing=run normal mode (jvm)
# $3 (need $1 adn $2 in true/false): true=set -x activated, false/nothing=does nothing


USE_MNV=$1
IS_NATIVE=$2
DEBUG_MODE=$3
DOCKER_COMPOSE_FILE="docker-compose.yaml"
if [ "$DEBUG_MODE" == "true" ]; then
  set -x
fi

if [ "$IS_NATIVE" == "true" ]; then
  echo "Native mode detected..."
  echo "Setting docker-compose-native.yaml for start the process..."
  DOCKER_COMPOSE_FILE="docker-compose-native.yaml"
fi

if [ "$USE_MNV" == "true" ]; then
  if [ "$IS_NATIVE" == "true" ]; then
    echo "Using ./mvnw clean package -Pnative..."
    #./mvnw clean package -Pnative
    mvn clean package -Pnative
  else
    echo "Using mvn clean package..."
    mvn clean package
  fi
fi

echo "Starting containers..."
docker-compose stop &&
  export DEBUG_LVL="INFO" &&
  export MONGO_URL="mongodb://block-store-db:27017" &&
  export MONGO_VOLUME="/proyecto/mongo/" &&
  docker-compose -f ./docker-compose-native.yaml up -d  --build &&
  docker ps
